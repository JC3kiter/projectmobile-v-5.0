package com.example.tic

import java.io.Serializable

class Producto(val nombre:String, val precio: Double, val descripcion: String, val imagen: Int): Serializable
