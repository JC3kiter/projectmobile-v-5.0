package com.example.tic

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_model_product.*

class ModelProductActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_model_product)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        val producto = intent.getSerializableExtra("producto") as Producto

        nombre_producto.text = producto.nombre
        precio_producto.text = "$${producto.precio}"
        detalles_producto.text = producto.descripcion
        imagen.setImageResource(producto.imagen)

    }
    }