package com.example.tic

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.synthetic.main.activity_lista_producto.*

class ListaProductoActivity : AppCompatActivity(),Parcelable {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_producto)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val producto = Producto("Batman 1", 100.0, "full HD", R.drawable.imagen1)
        val producto2 = Producto("Batman 2", 110.0, "HD", R.drawable.imagen2)

        val listaProductos = listOf(producto, producto2)

        val adapter = ProductosAdapter(this, listaProductos)

        lista.adapter = adapter

        lista.setOnItemClickListener { parent, view, position, id ->
            val intent = Intent(this, ModelProductActivity::class.java)
            intent.putExtra("producto", listaProductos[position])
            startActivity(intent)
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ListaProductoActivity> {
        override fun createFromParcel(parcel: Parcel): ListaProductoActivity {
            return ListaProductoActivity()
        }

        override fun newArray(size: Int): Array<ListaProductoActivity?> {
            return arrayOfNulls(size)
        }
    }
}