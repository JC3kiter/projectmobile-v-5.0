package com.example.projectshop.view.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.projectshop.R
import com.example.projectshop.databinding.FragmentFirstBinding
import com.example.projectshop.databinding.FragmentLogInBinding
import com.example.projectshop.databinding.FragmentSecondBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [FragmentLogIn.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentLogIn : Fragment() {
    // TODO: Rename and change types of parameters
    private var _binding: FragmentLogInBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentLogInBinding.inflate(inflater, container, false)
        return binding.root
    }
}